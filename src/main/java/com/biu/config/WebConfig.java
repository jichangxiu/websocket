package com.biu.config;

import org.atmosphere.cpr.AtmosphereServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfig {

    @Bean
    public ServletRegistrationBean servletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new AtmosphereServlet());
        registration.addInitParameter("org.atmosphere.cpr.packages", "com.biu.websocket");
        registration.addInitParameter("org.atmosphere.interceptor.HeartbeatInterceptor.clientHeartbeatFrequencyInSeconds", "60");
        registration.setLoadOnStartup(0);
        registration.setAsyncSupported(true);
        registration.addUrlMappings("/websocket/chat/*");
        return registration;
    }

}
