package com.biu.action;

import com.biu.websocket.Message;
import com.biu.websocket.WebSocketService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestAction {

    @RequestMapping("/getStr")
    public String getStr() {
        return "Hello World";
    }

    @RequestMapping(value = "/sendMsg/", method = RequestMethod.POST)
    public void sendMsg(Message msg) {
        System.out.println(msg);
        WebSocketService.send(msg);
    }

}
