package com.biu.websocket;

import cn.hutool.json.JSONUtil;

import java.io.Serializable;

public class WebSocketService implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = -6335171021702509140L;

    /**
     * 往客户端发送
     *
     * @param content
     */
    public static void send(Message content) {
        if (WebSocket.broadcaster != null) {
            String s = JSONUtil.toJsonStr(content);
            broadcast(s);
        }
    }

    /**
     * 广播消息
     *
     * @param content
     */
    public static void broadcast(String content) {
        if (WebSocket.broadcaster != null) {
            WebSocket.broadcaster.broadcast(content);
        }
    }
}

