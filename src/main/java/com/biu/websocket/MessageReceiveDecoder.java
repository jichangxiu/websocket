package com.biu.websocket;

import java.io.IOException;

import org.atmosphere.config.managed.Decoder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageReceiveDecoder implements Decoder<String, Message> {

    private ObjectMapper mapper = new ObjectMapper();

    public Message decode(String s) {
        try {
            return mapper.readValue(s, Message.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
